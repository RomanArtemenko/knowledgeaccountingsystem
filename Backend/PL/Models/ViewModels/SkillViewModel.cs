﻿using System.ComponentModel.DataAnnotations;

namespace WebApi.Models.ViewModels
{
    public class SkillViewModel
    {
        public int Id { get; set; }
        [Required] public string Name { get; set; }
        [Required] public string Description { get; set; }
    }
}
