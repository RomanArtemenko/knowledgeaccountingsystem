﻿using System.ComponentModel.DataAnnotations;

namespace WebApi.Models.ViewModels
{
    public class GroupViewModel
    {
        public int Id { get; set; }
        [Required] public string Name { get; set; }
    }
}
