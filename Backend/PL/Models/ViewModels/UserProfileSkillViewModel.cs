﻿using System.ComponentModel.DataAnnotations;

namespace WebApi.Models.ViewModels
{
    public class UserProfileSkillViewModel
    {
        [Required] public string Name { get; set; }
        [Required] public string Description { get; set; }
        [Required] public int KnowledgeLevel { get; set; }
    }
}
