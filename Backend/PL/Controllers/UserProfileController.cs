﻿using AutoMapper;
using BLL.Interfaces;
using BLL.Models.DTOs;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.IO;
using System.Security.Claims;
using System.Threading.Tasks;
using WebApi.Filters;
using WebApi.Models.ViewModels;

namespace WebApi.Controllers
{
    //[Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class UserProfileController : ControllerBase
    {
        private readonly IUserService _userService;
        private readonly ILogger<UserProfileController> _logger;
        private readonly IMapper _mapper;

        public UserProfileController(IUserService userService, ILogger<UserProfileController> logger, IMapper mapper)
        {
            _userService = userService;
            _logger = logger;
            _mapper = mapper;
        }

        [HttpGet("{userId}")]
        [ValidateModelState]
        public async Task<IActionResult> GetUserByIdAsync(string userId)
        {
            return Ok(_mapper.Map<UserViewModel>(await _userService.GetUserByIdAsync(userId)));
        }

        [HttpGet("skills/{userId}")]
        [ValidateModelState]
        public async Task<IActionResult> GetUserSkillsByUserIdAsync(string userId)
        {
            return Ok(_mapper.Map<UserProfileSkillViewModel>(await _userService.GetUserSkillsByUserIdAsync(userId)));
        }

        [HttpDelete("{userId}")]
        [ValidateModelState]
        public async Task<IActionResult> DeleteUserByIdAsync(string userId)
        {
            var claimsIdentity = User.Identity as ClaimsIdentity;
            var requestUserId = claimsIdentity.FindFirst("Id")?.Value;
            if (!string.IsNullOrEmpty(requestUserId) && requestUserId.Equals(userId))
            {
                await _userService.DeleteUserByUserIdAsync(userId);
                _logger.LogInformation($"User with ID: '{userId}' deleted!");
                return Ok();
            }
            return BadRequest("Invalid operation! UserId isn`t valid!");
        }

        [HttpPut("profile/{userId}")]
        [ValidateModelState]
        public async Task<IActionResult> UpdateUserProfileDataAsync(string userId, UserViewModel user)
        {
            await _userService.UpdateUserProfileDataAsync(userId, _mapper.Map<UserDTO>(user));
            _logger.LogInformation($"User with ID: '{userId}' updated!");
            return Ok();
        }

        [HttpPut("password/{userId}")]
        [ValidateModelState]
        public async Task<IActionResult> UpdateUserPasswordAsync(string userId, string currentPassword, string newPassword)
        {
            var claimsIdentity = User.Identity as ClaimsIdentity;
            var requestUserId = claimsIdentity.FindFirst("Id")?.Value;
            if (!string.IsNullOrEmpty(requestUserId) && requestUserId.Equals(userId))
            {
                await _userService.UpdateUserPasswordAsync(userId, currentPassword, newPassword);
                _logger.LogInformation($"Password of user with ID: '{userId}' updated!");
                return Ok();
            }
            return BadRequest("Invalid operation! UserId isn`t valid!");
        }

        [HttpPost("profileImage/{userId}")]
        [ValidateModelState]
        public async Task<IActionResult> UploadUserProfileImageAsync(string userId, [FromForm] IFormFile image)
        {
            var claimsIdentity = User.Identity as ClaimsIdentity;
            var requestUserId = claimsIdentity.FindFirst("Id")?.Value;
            if (!string.IsNullOrEmpty(requestUserId) && requestUserId.Equals(userId))
            {
                var stream = image.OpenReadStream();
                var buffer = new byte[stream.Length];
                await stream.ReadAsync(buffer, 0, buffer.Length).ConfigureAwait(false);

                await _userService.UploadUserProfileImageAsync(buffer, userId, Path.GetExtension(image.FileName), (int)image.Length);
                _logger.LogInformation($"Profile image of user with ID: '{userId}' was upload!");
                return Ok();
            }
            return BadRequest("Invalid operation! UserId isn`t valid!");
        }

        [HttpDelete("profileImage/{userId}")]
        [ValidateModelState]
        public async Task<IActionResult> RemoveUserProfileImageAsync(string userId)
        {
            await _userService.RemoveUserProfileImageAsync(userId);
            _logger.LogInformation($"Profile image of user with ID: '{userId}' deleted!");
            return Ok();
        }

        [HttpGet("profileImage/{userId}")]
        [ValidateModelState]
        public async Task<IActionResult> GetUserProfileImageAsync(string userId)
        {
            var profileImage = await _userService.GetUserProfileImageAsync(userId);
            return Ok(File(profileImage, "image/jpeg"));
        }

        [HttpPost("profileSkill/{userId}/{skillId}")]
        [ValidateModelState]
        public async Task<IActionResult> AttachSkillToUserAsync(string userId, int skillId, int knowledgeLevel)
        {
            await _userService.AttachSkillToUserAsync(skillId, userId, knowledgeLevel);
            _logger.LogInformation($"Skill with ID: {skillId} added to user with ID: '{userId}'!");
            return Ok();
        }

        [HttpDelete("profileSkill/{userId}/{skillId}")]
        [ValidateModelState]
        public async Task<IActionResult> RemoveSkillFromUserAsync(int skillId, string userId)
        {
            await _userService.RemoveSkillFromUserAsync(skillId, userId);
            _logger.LogInformation($"Skill with ID: {skillId} deleted from user with ID: '{userId}'!");
            return Ok();
        }

        [HttpPut("profileSkill/{userId}/{skillId}")]
        [ValidateModelState]
        public async Task<IActionResult> UpdateKnowledgeLevelOfUserSkillAsync(int skillId, string userId, int newKnowledgeLevel)
        {
            await _userService.UpdateKnowledgeLevelOfUserSkillAsync(skillId, userId, newKnowledgeLevel);
            _logger.LogInformation($"Skill with ID: {skillId} updated to user with ID: '{userId}'!");
            return Ok();
        }
    }
}
