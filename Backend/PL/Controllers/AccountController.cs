﻿using AutoMapper;
using BLL.Interfaces;
using BLL.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;
using WebApi.Filters;
using WebApi.Models.ViewModels;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly IUserService _userService;
        private readonly ILogger<AccountController> _logger;
        private readonly IMapper _mapper;

        public AccountController(IUserService userService, ILogger<AccountController> logger, IMapper mapper)
        {
            _userService = userService;
            _logger = logger;
            _mapper = mapper;
        }

        [HttpPost("login")]
        [ValidateModelState]
        public async Task<IActionResult> Login([FromBody] LoginViewModel loginModel)
        {
            var loginResult = await _userService.LoginAsync(_mapper.Map<LoginModel>(loginModel));
            _logger.LogInformation($"User with Email: {loginModel.Email} successfully logged in!");
            return Ok(loginResult.Token);
        }

        [HttpPost("register")]
        [ValidateModelState]
        public async Task<IActionResult> Register([FromBody] RegisterViewModel registerModel)
        {
            await _userService.RegisterAsync(_mapper.Map<RegisterModel>(registerModel));
            _logger.LogInformation($"User {registerModel.UserName} registered successfully!");
            return Ok();
        }

        [Authorize(Roles = "admin")]
        [HttpDelete("{userId}")]
        [ValidateModelState]
        public async Task<IActionResult> DeleteUserByIdAsync(string userId)
        {
            await _userService.DeleteUserByUserIdAsync(userId);
            _logger.LogInformation($"User with ID: '{userId}' deleted by admin!");
            return Ok();
        }

        [Authorize(Roles = "admin")]
        [HttpPut("addRole/{userId}/{role}")]
        [ValidateModelState]
        public async Task<IActionResult> AddUserToRoleByIdAsync(string userId, string role)
        {
            await _userService.AddUserToRoleAsync(userId, role);
            _logger.LogInformation($"User with ID: '{userId}' added to role: '{role}'!");
            return Ok();
        }

        [Authorize(Roles = "admin")]
        [HttpPut("removeRole/{userId}/{role}")]
        [ValidateModelState]
        public async Task<IActionResult> RemoveUserFromRoleByIdAsync(string userId, string role)
        {
            await _userService.RemoveUserFromRoleAsync(userId, role);
            _logger.LogInformation($"User with ID: '{userId}' removed from role: '{role}'!");
            return Ok();
        }
    }
}
