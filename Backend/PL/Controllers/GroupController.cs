﻿using AutoMapper;
using BLL.Interfaces;
using BLL.Models.DTOs;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Threading.Tasks;
using WebApi.Filters;
using WebApi.Models.ViewModels;

namespace WebApi.Controllers
{
    //[Authorize(Roles = "admin")]
    //[Authorize(Roles = "manager")]
    [Route("api/[controller]")]
    [ApiController]
    public class GroupController : ControllerBase
    {
        private readonly IGroupService _groupService;
        private readonly ILogger<GroupController> _logger;
        private readonly IMapper _mapper;

        public GroupController(IGroupService groupService, ILogger<GroupController> logger, IMapper mapper)
        {
            _groupService = groupService;
            _logger = logger;
            _mapper = mapper;
        }

        [HttpPost]
        [ValidateModelState]
        public async Task<IActionResult> AddGroupAsync([FromBody] GroupViewModel groupModel)
        {
            await _groupService.AddGroupAsync(_mapper.Map<GroupDTO>(groupModel));
            _logger.LogInformation($"Group: {groupModel.Name} successfully created!");
            return Ok();
        }

        [HttpDelete("{groupId}")]
        [ValidateModelState]
        public async Task<IActionResult> DeleteGroupByIdAsync(int groupId)
        {
            await _groupService.DeleteGroupByIdAsync(groupId);
            _logger.LogInformation($"Group with ID: {groupId} deleted!");
            return Ok();
        }

        [Authorize(Roles = "programmer")]
        [HttpGet]
        [ValidateModelState]
        public async Task<IActionResult> GetGroupsAsync()
        {
            return Ok(_mapper.Map<ICollection<GroupViewModel>>(await _groupService.GetGroupsAsync()));
        }

        [HttpPut]
        [ValidateModelState]
        public async Task<IActionResult> UpdateGroupAsync([FromBody] GroupViewModel groupModel)
        {
            await _groupService.UpdateGroupAsync(_mapper.Map<GroupDTO>(groupModel));
            _logger.LogInformation($"Group: {groupModel.Name} updated!");
            return Ok();
        }

        [HttpGet("skills/{groupId}")]
        [ValidateModelState]
        public async Task<IActionResult> GetSkillsByGroupIdAsync(int groupId)
        {
            return Ok(_mapper.Map<ICollection<SkillViewModel>>(await _groupService.GetSkillsByGroupIdAsync(groupId)));
        }
    }
}
