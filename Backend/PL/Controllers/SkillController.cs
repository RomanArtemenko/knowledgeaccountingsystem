﻿using AutoMapper;
using BLL.Interfaces;
using BLL.Models.DTOs;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Threading.Tasks;
using WebApi.Filters;
using WebApi.Models.ViewModels;


namespace WebApi.Controllers
{
    //[Authorize(Roles = "admin")]
    //[Authorize(Roles = "manager")]
    [Route("api/[controller]")]
    [ApiController]
    public class SkillController : ControllerBase
    {
        private readonly ISkillService _skillService;
        private readonly ILogger<SkillController> _logger;
        private readonly IMapper _mapper;

        public SkillController(ISkillService skillService, ILogger<SkillController> logger, IMapper mapper)
        {
            _skillService = skillService;
            _logger = logger;
            _mapper = mapper;
        }

        [HttpPost]
        [ValidateModelState]
        public async Task<IActionResult> AddSkillAsync([FromBody] SkillViewModel skillModel)
        {
            await _skillService.AddSkillAsync(_mapper.Map<SkillDTO>(skillModel));
            _logger.LogInformation($"Skill: {skillModel.Name} successfully created!");
            return Ok();
        }

        [HttpDelete("{skillId}")]
        [ValidateModelState]
        public async Task<IActionResult> DeleteSkillByIdAsync(int skillId)
        {
            await _skillService.DeleteSkillByIdAsync(skillId);
            _logger.LogInformation($"Skill with ID: {skillId} deleted!");
            return Ok();
        }

        [Authorize(Roles = "programmer")]
        [HttpGet]
        [ValidateModelState]
        public async Task<IActionResult> GetSkillsAsync()
        {
            return Ok(_mapper.Map<ICollection<SkillViewModel>>(await _skillService.GetSkillsAsync()));
        }

        [HttpPut]
        [ValidateModelState]
        public async Task<IActionResult> UpdateSkillAsync([FromBody] SkillViewModel skillModel)
        {
            await _skillService.UpdateSkillAsync(_mapper.Map<SkillDTO>(skillModel));
            _logger.LogInformation($"Group: {skillModel.Name} updated!");
            return Ok();
        }
    }
}
