﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using BLL.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WebApi.Filters;
using WebApi.Models.ViewModels;

namespace WebApi.Controllers
{
    //[Authorize(Roles = "admin")]
    //[Authorize(Roles = "manager")]
    [Route("api/[controller]")]
    [ApiController]
    public class ManagerController : ControllerBase
    {
        private readonly IUserService _userService;
        private readonly IReportService _reportService;
        private readonly IMapper _mapper;

        public ManagerController(IUserService userService, IMapper mapper, IReportService reportService)
        {
            _userService = userService;
            _mapper = mapper;
            _reportService = reportService;
        }

        [HttpGet("user/{email}")]
        [ValidateModelState]
        public async Task<IActionResult> GetUserByEmailAsync(string email)
        {
            return Ok(_mapper.Map<UserViewModel>(await _userService.GetUserByEmailAsync(email)));
        }

        [HttpGet("users")]
        [ValidateModelState]
        public async Task<IActionResult> GetUserAsync()
        {
            return Ok(_mapper.Map<ICollection<UserViewModel>>(await _userService.GetUsersAsync()));
        }

        [HttpGet("usersBySkill/{skillId}/{knowledgeLevel}")]
        [ValidateModelState]
        public async Task<IActionResult> GetProgrammersBySkillAsync(int skillId, int knowledgeLevel)
        {
            return Ok(_mapper.Map<ICollection<UserViewModel>>(await _userService.GetProgrammersBySkillAsync(skillId, knowledgeLevel)));
        }

        [HttpGet("report/{skillId}/{knowledgeLevel}")]
        [ValidateModelState]
        public async Task<IActionResult> GetReportAsync(int skillId, int knowledgeLevel)
        {
            return Ok(File(await _reportService.GenerateReport(await _userService.GetProgrammersBySkillAsync(skillId, knowledgeLevel)), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"));
        }
    }
}
