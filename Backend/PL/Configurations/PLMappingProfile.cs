﻿using AutoMapper;
using BLL.Models;
using BLL.Models.DTOs;
using WebApi.Models.ViewModels;

namespace WebApi.Configurations
{
    public class PLMappingProfile : Profile
    {
        public PLMappingProfile()
        {
            CreateMap<LoginModel, LoginViewModel>()
                .ReverseMap();

            CreateMap<UserDTO, UserViewModel>()
                .ReverseMap();

            CreateMap<RegisterModel, RegisterViewModel>()
                .ReverseMap();

            CreateMap<GroupDTO, GroupViewModel>()
                .ReverseMap();

            CreateMap<SkillDTO, SkillViewModel>()
                .ReverseMap();

            CreateMap<UserProfileSkillDTO, UserProfileSkillViewModel>();
        }
    }
}
