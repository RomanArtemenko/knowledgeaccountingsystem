﻿using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;

namespace DAL.Models
{
    public class User : IdentityUser
    {
        public string ProfileImageId { get; set; }
        public virtual ICollection<UserSkill> UserSkills { get; set; }
    }
}
