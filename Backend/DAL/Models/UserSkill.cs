﻿namespace DAL.Models
{
    public class UserSkill
    {
        public int KnowledgeLevel { get; set; }
        public string UserId { get; set; }
        public int SkillId { get; set; }

        public Skill Skill { get; set; }
        public User User { get; set; }
    }
}
