﻿using System.Collections.Generic;

namespace DAL.Models
{
    public class Group
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public ICollection<Skill> Skills { get; set; }
    }
}
