﻿using System.Collections.Generic;

namespace DAL.Models
{
    public class Skill
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int GroupId { get; set; }

        public Group Group { get; set; }
        public ICollection<UserSkill> UserSkills { get; set; }
    }
}
