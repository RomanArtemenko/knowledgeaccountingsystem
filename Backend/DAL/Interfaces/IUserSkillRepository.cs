﻿using DAL.Models;
using System;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace DAL.Interfaces
{
    public interface IUserSkillRepository : IRepository<UserSkill>
    {
        Task<UserSkill> FirstOrDefaultAsync(Expression<Func<UserSkill, bool>> predicate);
    }
}
