﻿using System.Threading.Tasks;

namespace DAL.Interfaces
{
    public interface IUnitOfWork
    {
        IGroupRepository Groups { get; }
        ISkillRepository Skills { get; }
        IUserSkillRepository UserSkills { get; }

        Task SaveAsync();
    }
}
