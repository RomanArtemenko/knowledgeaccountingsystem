﻿using DAL.Models;

namespace DAL.Interfaces
{
    public interface IGroupRepository : IRepository<Group>
    {
    }
}
