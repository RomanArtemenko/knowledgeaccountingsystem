﻿using DAL.Models;

namespace DAL.Interfaces
{
    public interface ISkillRepository : IRepository<Skill>
    {
    }
}
