﻿using DAL.Interfaces;
using DAL.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace DAL.Repositories
{
    public class UserSkillRepository : BaseRepository<UserSkill>, IUserSkillRepository
    {
        public UserSkillRepository(DbContext context) : base(context)
        {
        }

        public async Task<UserSkill> FirstOrDefaultAsync(Expression<Func<UserSkill, bool>> predicate)
        {
            return await _entity.FirstOrDefaultAsync(predicate);
        }
    }
}
