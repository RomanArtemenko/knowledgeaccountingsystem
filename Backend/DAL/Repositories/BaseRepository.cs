﻿using DAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace DAL.Repositories
{
    public abstract class BaseRepository<TEntity> : IRepository<TEntity>
        where TEntity : class
    {
        protected readonly DbContext _context;
        protected readonly DbSet<TEntity> _entity;

        protected BaseRepository(DbContext context)
        {
            _context = context;
            _entity = _context.Set<TEntity>();
        }

        public virtual void Create(TEntity entity)
        {
            _entity.Add(entity);
        }

        public virtual void Delete(TEntity entity)
        {
            _entity.Remove(entity);
        }

        public virtual async Task DeleteByIdAsync(int id)
        {
            _entity.Remove(await _entity.FindAsync(id));
        }

        public virtual void Update(TEntity entity)
        {
            _entity.Update(entity);
        }

        public virtual IQueryable<TEntity> GetAll()
        {
            return _entity.AsNoTracking();
        }

        public virtual async Task<ICollection<TEntity>> GetAllAsync()
        {
            return await _entity.ToListAsync();
        }

        public virtual IQueryable<TEntity> GetAllIncluding(params Expression<Func<TEntity, object>>[] includeProperties)
        {
            var queryable = GetAll();
            foreach (var includeProperty in includeProperties)
                queryable = queryable.Include(includeProperty);
            return queryable;
        }

        public virtual async Task<ICollection<TEntity>> GetAllIncludingAsync(params Expression<Func<TEntity, object>>[] includeProperties)
        {
            return await GetAllIncluding(includeProperties).ToListAsync();
        }

        public virtual async Task<TEntity> GetByIdAsync(int key)
        {
            return await _entity.FindAsync(key);
        }

        public virtual IQueryable<TEntity> GetWhere(Expression<Func<TEntity, bool>> expression)
        {
            return _entity.AsNoTracking().Where(expression);
        }

        public virtual async Task<ICollection<TEntity>> GetWhereAsync(Expression<Func<TEntity, bool>> expression)
        {
            return await GetWhere(expression).ToListAsync();
        }
    }
}
