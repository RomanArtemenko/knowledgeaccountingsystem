﻿using DAL.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DAL.Configurations
{
    public class UserSkillConfiguration : IEntityTypeConfiguration<UserSkill>
    {
        public void Configure(EntityTypeBuilder<UserSkill> builder)
        {
            builder.HasKey(ck => new { ck.UserId, ck.SkillId });

            builder.Property(p => p.KnowledgeLevel)
                    .IsRequired();

            builder.HasOne(p => p.User)
                   .WithMany(p => p.UserSkills)
                   .HasForeignKey(p => p.UserId)
                   .OnDelete(DeleteBehavior.Cascade);

            builder.HasOne(p => p.Skill)
                   .WithMany(p => p.UserSkills)
                   .HasForeignKey(p => p.SkillId)
                   .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
