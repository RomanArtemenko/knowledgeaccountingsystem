﻿using DAL.Interfaces;
using DAL.Repositories;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace DAL.Context
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly DbContext _dbContext;

        private IGroupRepository _groupRepository;
        private ISkillRepository _skillRepository;
        private IUserSkillRepository _userSkillRepository;

        public UnitOfWork(KnowledgeAccountingSystemDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public IGroupRepository Groups
        {
            get
            {
                if (_groupRepository == null)
                    _groupRepository = new GroupRepository(_dbContext);
                return _groupRepository;
            }
        }

        public ISkillRepository Skills
        {
            get
            {
                if (_skillRepository == null)
                    _skillRepository = new SkillRepository(_dbContext);
                return _skillRepository;
            }
        }

        public IUserSkillRepository UserSkills
        {
            get
            {
                if (_userSkillRepository == null)
                    _userSkillRepository = new UserSkillRepository(_dbContext);
                return _userSkillRepository;
            }
        }

        public async Task SaveAsync()
        {
            await _dbContext.SaveChangesAsync();
        }
    }
}
