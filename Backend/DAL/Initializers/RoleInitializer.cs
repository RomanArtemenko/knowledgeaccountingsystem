﻿using DAL.Models;
using Microsoft.AspNetCore.Identity;
using System.Threading.Tasks;

namespace DAL.Initializers
{
    public class RoleInitializer
    {
        public static async Task InitializeAsync(UserManager<User> userManager, RoleManager<IdentityRole> roleManager)
        {
            string adminEmail = "admin@gmail.com";
            string password = "adminn";

            if (await roleManager.FindByNameAsync("admin") == null)
                await roleManager.CreateAsync(new IdentityRole("admin"));

            if (await roleManager.FindByNameAsync("manager") == null)
                await roleManager.CreateAsync(new IdentityRole("manager"));

            if (await roleManager.FindByNameAsync("programmer") == null)
                await roleManager.CreateAsync(new IdentityRole("programmer"));

            if (await userManager.FindByNameAsync(adminEmail) == null)
            {
                User admin = new User { Email = adminEmail, UserName = adminEmail };
                IdentityResult result = await userManager.CreateAsync(admin, password);
                if (result.Succeeded)
                {
                    await userManager.AddToRoleAsync(admin, "admin");
                }
            }
        }
    }
}
