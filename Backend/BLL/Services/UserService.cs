﻿using AutoMapper;
using BLL.Exception;
using BLL.Interfaces;
using BLL.Models;
using BLL.Models.DTOs;
using DAL.ImageFileStreamStorage.Interfaces;
using DAL.Interfaces;
using DAL.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Services
{
    public class UserService : IUserService
    {
        private readonly UserManager<User> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly IUnitOfWork _database;
        private readonly IFileStorageContext _fileStorage;
        private readonly IMapper _mapper;
        private readonly JwtSettings _jwtSettings;

        public UserService(UserManager<User> userManager,
                           RoleManager<IdentityRole> roleManager,
                           IUnitOfWork unitOfWork,
                           IFileStorageContext fileStorageContext,
                           IMapper mapper,
                           JwtSettings jwtSettings)
        {
            _userManager = userManager;
            _roleManager = roleManager;
            _database = unitOfWork;
            _fileStorage = fileStorageContext;
            _mapper = mapper;
            _jwtSettings = jwtSettings;
        }

        public async Task RegisterAsync(RegisterModel registerModelDTO)
        {
            if (string.IsNullOrEmpty(registerModelDTO.UserName))
                throw new ServiceException("Username is NULL or EMPTY!");
            if (string.IsNullOrEmpty(registerModelDTO.Email))
                throw new ServiceException("Email is NULL or EMPTY!");
            if (string.IsNullOrEmpty(registerModelDTO.Password))
                throw new ServiceException("Password is NULL or EMPTY!");

            var existingUser = await _userManager.FindByEmailAsync(registerModelDTO.Email);
            if (existingUser != null)
                throw new ServiceException($"User with email: '{registerModelDTO.Email}' already exists!");

            var newUser = new User { Email = registerModelDTO.Email, UserName = registerModelDTO.UserName };
            var identityUserResult = await _userManager.CreateAsync(newUser, registerModelDTO.Password);
            if (!identityUserResult.Succeeded)
                throw new ServiceException($"User with email: '{registerModelDTO.Email}' NOT created!");
            var identityRoleResult = await _userManager.AddToRoleAsync(newUser, "programmer");
            if (!identityRoleResult.Succeeded)
                throw new ServiceException("User can`t be added to role!");
        }

        public async Task AddUserToRoleAsync(string userId, string role)
        {
            if (string.IsNullOrEmpty(userId))
                throw new ServiceException("Incorrect user ID!");
            if (string.IsNullOrEmpty(role))
                throw new ServiceException("Incorrect role Name!");
            if (role.Equals("admin"))
                throw new ServiceException($"Can`t add user to role: '{role}'!");

            var user = await _userManager.FindByIdAsync(userId);
            if (user is null)
                throw new ServiceException($"User with Id: {userId} isn`t exist!");

            var currentRole = (await _userManager.GetRolesAsync(user)).FirstOrDefault();
            var removeFromCurrentRoleResult = await _userManager.RemoveFromRoleAsync(user, currentRole);
            if (!removeFromCurrentRoleResult.Succeeded)
                throw new ServiceException($"Can`t delete user from current role: '{currentRole}'!");

            var dbRole = await _roleManager.FindByNameAsync(role);
            if (dbRole is null)
                throw new ServiceException($"Role with Name: '{role}' not found!");

            var identityResult = await _userManager.AddToRoleAsync(user, dbRole.Name);
            if (!identityResult.Succeeded)
                throw new ServiceException($"User can`t be added to role: '{dbRole.Name}'!");
        }

        public async Task RemoveUserFromRoleAsync(string userId, string role)
        {
            if (string.IsNullOrEmpty(userId))
                throw new ServiceException("Incorrect user ID!");
            if (string.IsNullOrEmpty(role))
                throw new ServiceException("Incorrect role Name!");
            if (role.Equals("admin"))
                throw new ServiceException($"Can`t delete user from role: '{role}'!");

            var user = await _userManager.FindByIdAsync(userId);
            if (user is null)
                throw new ServiceException($"User with Id: {userId} isn`t exist!");

            var dbRole = await _roleManager.FindByNameAsync(role);
            if (dbRole is null)
                throw new ServiceException($"Role with Name: '{role}' not found!");

            var userRoles = await _userManager.GetRolesAsync(user);
            if (!userRoles.Contains(dbRole.Name))
                throw new ServiceException($"User don`t have role: '{dbRole.Name}'!");

            var identityResult = await _userManager.RemoveFromRoleAsync(user, dbRole.Name);
            if (!identityResult.Succeeded)
                throw new ServiceException($"User can`t be removed from role: '{dbRole.Name}'!");
        }

        public async Task<AuthenticationResult> LoginAsync(LoginModel loginData)
        {
            if (string.IsNullOrEmpty(loginData.Email))
                throw new ServiceException("Email is NULL or EMPTY!");
            if (string.IsNullOrEmpty(loginData.Password))
                throw new ServiceException("Password is NULL or EMPTY!");

            var user = await _userManager.FindByEmailAsync(loginData.Email).ConfigureAwait(false);
            if (user is null)
                throw new ServiceException($"User with email: {loginData.Email} - does not exists!");

            var userHasValidPassword = await _userManager.CheckPasswordAsync(user, loginData.Password).ConfigureAwait(false);
            if (!userHasValidPassword)
                throw new ServiceException("User has incorrect password!");

            var roleName = (await _userManager.GetRolesAsync(user)).FirstOrDefault();

            return GenerateAuthenticationResultForUser(user, roleName);
        }

        public async Task<UserDTO> GetUserByEmailAsync(string email)
        {
            if (string.IsNullOrEmpty(email))
                throw new ServiceException("Incorrect user email!");

            return _mapper.Map<UserDTO>(await _userManager.FindByEmailAsync(email));
        }

        public async Task<UserProfileSkillDTO> GetUserSkillsByUserIdAsync(string userId)
        {
            if (string.IsNullOrEmpty(userId))
                throw new ServiceException("Incorrect user Id!");

            return _mapper.Map<UserProfileSkillDTO>(await _database.UserSkills.GetWhereAsync(p => p.UserId.Equals(userId)));
        }

        public async Task<ICollection<UserDTO>> GetUsersAsync()
        {
            return _mapper.Map<ICollection<UserDTO>>(await _userManager.Users.ToListAsync());
        }

        public async Task<UserDTO> GetUserByIdAsync(string userId)
        {
            if (string.IsNullOrEmpty(userId))
                throw new ServiceException("Incorrect user ID!");

            return _mapper.Map<UserDTO>(await _userManager.FindByIdAsync(userId));
        }

        public async Task DeleteUserByUserIdAsync(string userId)
        {
            if (string.IsNullOrEmpty(userId))
                throw new ServiceException("Incorrect user ID!");

            var user = await _userManager.FindByIdAsync(userId);
            if (user is null)
                throw new ServiceException($"User with Id: {userId} isn`t exist!");

            var identityResult = await _userManager.DeleteAsync(user);
            if (!identityResult.Succeeded)
                throw new ServiceException("User NOT deleted!");
        }

        public async Task UpdateUserProfileDataAsync(string userId, UserDTO user)
        {
            if (user is null)
                throw new ServiceException("User is null!");
            if (string.IsNullOrEmpty(userId))
                throw new ServiceException("Incorrect user ID!");
            if (!userId.Equals(user.Id))
                throw new ServiceException($"User.Id and userId don`t match!");

            var userToUpdate = await _userManager.FindByIdAsync(user.Id);
            if (userToUpdate is null)
                throw new ServiceException($"User with Id: {user.Id} isn`t exist!");

            if (!string.IsNullOrEmpty(user.UserName))
                userToUpdate.UserName = user.UserName;
            if (!string.IsNullOrEmpty(user.Email))
                userToUpdate.Email = user.Email;
            if (!string.IsNullOrEmpty(user.PhoneNumber))
                userToUpdate.PhoneNumber = user.PhoneNumber;

            var identityResult = await _userManager.UpdateAsync(userToUpdate);
            if (!identityResult.Succeeded)
                throw new ServiceException("User NOT updated!");
        }

        public async Task UpdateUserPasswordAsync(string userId, string currentPassword, string newPassword)
        {
            if (string.IsNullOrEmpty(userId))
                throw new ServiceException("Incorrect user ID!");
            if (String.IsNullOrEmpty(currentPassword))
                throw new ServiceException("Current password is null or empty!");
            if (String.IsNullOrEmpty(newPassword))
                throw new ServiceException("New password is null or empty!");

            var userToUpdate = await _userManager.FindByIdAsync(userId);
            if (userToUpdate is null)
                throw new ServiceException($"User with Id: {userId} isn`t exist!");

            var identityResult = await _userManager.ChangePasswordAsync(userToUpdate, currentPassword, newPassword);
            if (!identityResult.Succeeded)
                throw new ServiceException($"Password NOT updated! {identityResult.Errors}");
        }

        public async Task UploadUserProfileImageAsync(byte[] fileData, string userId, string fileType, int fileSize)
        {
            string[] accessTypeFiles = {".jpeg", ".jpg" };
            if (!accessTypeFiles.Contains(fileType))
                throw new ServiceException($"Your file must have a format: {string.Join(", ", accessTypeFiles)}");

            int maxFileSize = 1024 * 1024 * 2;
            if (fileSize > maxFileSize)
                throw new ServiceException("Please upload a file up to 2 mb");

            if (string.IsNullOrEmpty(userId))
                throw new ServiceException("Incorrect user ID!");

            var user = await _userManager.FindByIdAsync(userId);
            if (user is null)
                throw new ServiceException($"User with Id: {userId} isn`t exist!");
            if (!string.IsNullOrEmpty(user.ProfileImageId))
                _fileStorage.DeleteFile(user.ProfileImageId);

            var fileName = Guid.NewGuid().ToString();

            await _fileStorage.SaveFile(fileData, fileName);

            user.ProfileImageId = fileName;
            var identityResult = await _userManager.UpdateAsync(user);
            if (!identityResult.Succeeded)
                throw new ServiceException("ProfileImageId NOT updated!");
        }

        public async Task RemoveUserProfileImageAsync(string userId)
        {
            if (string.IsNullOrEmpty(userId))
                throw new ServiceException("Incorrect user ID!");

            var user = await _userManager.FindByIdAsync(userId);
            if (user is null)
                throw new ServiceException($"User with Id: {userId} isn`t exist!");

            _fileStorage.DeleteFile(user.ProfileImageId);

            user.ProfileImageId = null;
            var identityResult = await _userManager.UpdateAsync(user);
            if (!identityResult.Succeeded)
                throw new ServiceException("ProfileImageId NOT deleted!");
        }

        public async Task<byte[]> GetUserProfileImageAsync(string userId)
        {
            if (string.IsNullOrEmpty(userId))
                throw new ServiceException("Incorrect user ID!");

            var user = await _userManager.FindByIdAsync(userId);
            if (user is null)
                throw new ServiceException($"User with Id: {userId} isn`t exist!");

            return await _fileStorage.ReadFile(user.ProfileImageId);
        }

        public async Task<ICollection<UserDTO>> GetProgrammersBySkillAsync(int skillId, int knowledgeLevel)
        {
            if (knowledgeLevel < 0 || knowledgeLevel > 10)
                throw new ServiceException("Incorrect knowledge lewel value!");

            if (await _database.Skills.GetByIdAsync(skillId) is null)
                throw new ServiceException($"Skill with Id: {skillId} isn`t exist!");

            var userSkills = await _database.UserSkills.GetAllIncludingAsync(p => p.User);

            return _mapper.Map<ICollection<UserDTO>>(userSkills.Where(p => p.SkillId == skillId && p.KnowledgeLevel >= knowledgeLevel)
                                                               .Select(p => p.User));
        }

        public async Task AttachSkillToUserAsync(int skillId, string userId, int knowledgeLevel)
        {
            if (knowledgeLevel < 0 || knowledgeLevel > 10)
                throw new ServiceException("Incorrect knowledge lewel value!");
            if (string.IsNullOrEmpty(userId))
                throw new ServiceException("Incorrect user ID!");

            var user = await _userManager.FindByIdAsync(userId);
            if (user is null)
                throw new ServiceException($"User with Id: {userId} isn`t exist!");

            var skill = await _database.Skills.GetByIdAsync(skillId);
            if (skill is null)
                throw new ServiceException($"Skill with Id: {skillId} isn`t exist!");

            _database.UserSkills.Create(new UserSkill { UserId = userId, SkillId = skillId, KnowledgeLevel = knowledgeLevel });
            await _database.SaveAsync();
        }

        public async Task RemoveSkillFromUserAsync(int skillId, string userId)
        {
            if (string.IsNullOrEmpty(userId))
                throw new ServiceException("Incorrect user ID!");

            var user = await _userManager.FindByIdAsync(userId);
            if (user is null)
                throw new ServiceException($"User with Id: {userId} isn`t exist!");

            var skill = await _database.Skills.GetByIdAsync(skillId);
            if (skill is null)
                throw new ServiceException($"Skill with Id: {skillId} isn`t exist!");

            var userSkill = await _database.UserSkills.FirstOrDefaultAsync(p => p.SkillId == skillId && p.UserId.Equals(userId));
            if (userSkill is null)
                throw new ServiceException($"User don`t have {skill.Name} skill!");

            _database.UserSkills.Delete(userSkill);
            await _database.SaveAsync();
        }

        public async Task UpdateKnowledgeLevelOfUserSkillAsync(int skillId, string userId, int newKnowledgeLevel)
        {
            if (newKnowledgeLevel < 0 || newKnowledgeLevel > 10)
                throw new ServiceException("Incorrect knowledge lewel value!");
            if (string.IsNullOrEmpty(userId))
                throw new ServiceException("Incorrect user ID!");

            var user = await _userManager.FindByIdAsync(userId);
            if (user is null)
                throw new ServiceException($"User with Id: {userId} isn`t exist!");

            var skill = await _database.Skills.GetByIdAsync(skillId);
            if (skill is null)
                throw new ServiceException($"Skill with Id: {skillId} isn`t exist!");

            var userSkill = await _database.UserSkills.FirstOrDefaultAsync(p => p.SkillId == skillId && p.UserId.Equals(userId));
            if (userSkill is null)
                throw new ServiceException($"User don`t have {skill.Name} skill!");

            userSkill.KnowledgeLevel = newKnowledgeLevel;
            await _database.SaveAsync();
        }

        private AuthenticationResult GenerateAuthenticationResultForUser(User newUser, string roleName)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_jwtSettings.Secret);

            List<Claim> claims = new List<Claim>()
            {
                new Claim(JwtRegisteredClaimNames.Sub, newUser.Email),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim(JwtRegisteredClaimNames.Email, newUser.Email),
                new Claim("id", newUser.Id),
                new Claim(ClaimsIdentity.DefaultRoleClaimType, roleName)
            };

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(claims),
                Expires = DateTime.UtcNow.AddHours(1),
                SigningCredentials =
                    new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return new AuthenticationResult { Token = tokenHandler.WriteToken(token) };
        }
    }
}
