﻿using AutoMapper;
using BLL.Exception;
using BLL.Interfaces;
using BLL.Models.DTOs;
using DAL.Interfaces;
using DAL.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BLL.Services
{
    public class GroupService : IGroupService
    {
        private readonly IUnitOfWork _database;
        private readonly IMapper _mapper;

        public GroupService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _database = unitOfWork;
            _mapper = mapper;
        }

        public async Task AddGroupAsync(GroupDTO group)
        {
            if (group is null)
                throw new ServiceException("Group is null");

            _database.Groups.Create(_mapper.Map<Group>(group));
            await _database.SaveAsync();
        }

        public async Task DeleteGroupByIdAsync(int groupId)
        {
            if (groupId <= 0)
                throw new ServiceException($"Incorrect group Id: {groupId} !");

            var group = await _database.Groups.GetByIdAsync(groupId);
            if (group is null)
                throw new ServiceException($"Group with Id: {groupId} isn`t exist!");

            _database.Groups.Delete(group);
            await _database.SaveAsync();
        }

        public async Task<ICollection<GroupDTO>> GetGroupsAsync()
        {
            return _mapper.Map<ICollection<GroupDTO>>(await _database.Groups.GetAllAsync());
        }

        public async Task<ICollection<SkillDTO>> GetSkillsByGroupIdAsync(int groupId)
        {
            if (groupId <= 0)
                throw new ServiceException($"Incorrect group Id: {groupId} !");
            if (await _database.Groups.GetByIdAsync(groupId) is null)
                throw new ServiceException($"Group with Id: {groupId} isn`t exist!");

            return _mapper.Map<ICollection<SkillDTO>>(await _database.Skills.GetWhereAsync(p => p.GroupId == groupId));
        }

        public async Task UpdateGroupAsync(GroupDTO group)
        {
            if (group is null)
                throw new ServiceException("Group is null");

            var groupFromDb = await _database.Groups.GetByIdAsync(group.Id);
            if (groupFromDb is null)
                throw new ServiceException($"Group with Id: {group.Id} isn`t exist!");

            _database.Groups.Update(_mapper.Map<Group>(group));
            await _database.SaveAsync();
        }
    }
}
