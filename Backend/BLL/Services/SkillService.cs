﻿using AutoMapper;
using BLL.Exception;
using BLL.Interfaces;
using BLL.Models.DTOs;
using DAL.Interfaces;
using DAL.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BLL.Services
{
    public class SkillService : ISkillService
    {
        private readonly IUnitOfWork _database;
        private readonly IMapper _mapper;

        public SkillService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _database = unitOfWork;
            _mapper = mapper;
        }

        public async Task AddSkillAsync(SkillDTO skill)
        {
            if (skill is null)
                throw new ServiceException("Skill is null");

            _database.Skills.Create(_mapper.Map<Skill>(skill));
            await _database.SaveAsync();
        }

        public async Task DeleteSkillByIdAsync(int skillId)
        {
            if (skillId <= 0)
                throw new ServiceException($"Incorrect skill Id: {skillId} !");

            var skill = await _database.Skills.GetByIdAsync(skillId);
            if (skill is null)
                throw new ServiceException($"Skill with Id: {skillId} isn`t exist!");

            _database.Skills.Delete(skill);
            await _database.SaveAsync();
        }

        public async Task<ICollection<SkillDTO>> GetSkillsAsync()
        {
            return _mapper.Map<ICollection<SkillDTO>>(await _database.Skills.GetAllAsync());
        }

        public async Task UpdateSkillAsync(SkillDTO skill)
        {
            if (skill is null)
                throw new ServiceException("Skill is null");

            var skillFromDb = await _database.Groups.GetByIdAsync(skill.Id);
            if (skillFromDb is null)
                throw new ServiceException($"Skill with Id: {skill.Id} isn`t exist!");

            _database.Skills.Update(_mapper.Map<Skill>(skill));
            await _database.SaveAsync();
        }
    }
}
