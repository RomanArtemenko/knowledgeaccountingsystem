﻿using BLL.Exception;
using BLL.Interfaces;
using BLL.Models.DTOs;
using DAL.Interfaces;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;

namespace BLL.Services
{
    public class ReportService : IReportService
    {
        private readonly IUnitOfWork _database;
        public ReportService(IUnitOfWork unitOfWork)
        {
            _database = unitOfWork;
        }

        public async Task<byte[]> GenerateReport(ICollection<UserDTO> users)
        {
            ExcelFill fill;
            Border border;

            if (users is null)
                throw new ServiceException("UserList is NULL!");
            if (!users.Any())
                throw new ServiceException("UserList is EMPTY!");

            int firstRow = 1;
            int endRow = users.Count + firstRow;

            foreach (var user in users)
            {
                endRow += user.SkillsIds.Count;
            }

            using (ExcelPackage excelPackage = new ExcelPackage())
            {
                ExcelWorksheet sheet = excelPackage.Workbook.Worksheets.Add("Users Excel Report");
                ExcelRange cell = sheet.Cells[$"A{firstRow}:F{endRow}"];
                excelPackage.Workbook.Properties.Author = "Manager";
                excelPackage.Workbook.Properties.Title = "Report";
                excelPackage.Workbook.Properties.Created = DateTime.Now;

                cell.Style.Font.Bold = true;
                cell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                cell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                fill = cell.Style.Fill;
                fill.PatternType = ExcelFillStyle.Solid;
                fill.BackgroundColor.SetColor(Color.White);
                border = cell.Style.Border;
                border.Bottom.Style = border.Top.Style = border.Left.Style = border.Right.Style = ExcelBorderStyle.Thin;

                cell = sheet.Cells["A1"];
                cell.Value = "User Name";
                cell = sheet.Cells["B1"];
                cell.Value = "Email";
                cell = sheet.Cells["C1"];
                cell.Value = "Phone Number";
                cell = sheet.Cells["D1"];
                cell.Value = "Skill";
                cell = sheet.Cells["E1"];
                cell.Value = "Knowledge Level";

                int currentRow = firstRow + 1;

                foreach (var user in users)
                {
                    cell = sheet.Cells[$"A{currentRow}"];
                    cell.Value = user.UserName;
                    cell = sheet.Cells[$"B{currentRow}"];
                    cell.Value = user.Email;
                    cell = sheet.Cells[$"C{currentRow}"];
                    cell.Value = user.PhoneNumber;

                    foreach (var skillId in user.SkillsIds)
                    {
                        cell = sheet.Cells[$"D{currentRow}"];
                        cell.Value = (await _database.Skills.GetByIdAsync(skillId)).Name;
                        cell = sheet.Cells[$"E{currentRow}"];
                        cell.Value = (await _database.UserSkills.FirstOrDefaultAsync(p => p.SkillId == skillId && p.UserId.Equals(user.Id))).KnowledgeLevel;

                        currentRow++;
                    }
                }
                sheet.Cells[sheet.Dimension.Address].AutoFitColumns();
                return excelPackage.GetAsByteArray();
            }
        }
    }
}
