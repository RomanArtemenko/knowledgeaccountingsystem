﻿namespace BLL.Models.DTOs
{
    public class UserProfileSkillDTO
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public int KnowledgeLevel { get; set; }
    }
}
