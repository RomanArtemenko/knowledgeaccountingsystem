﻿using System.Collections.Generic;

namespace BLL.Models.DTOs
{
    public class GroupDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public ICollection<int> SkillsIds { get; set; }
    }
}
