﻿using System.Collections.Generic;

namespace BLL.Models.DTOs
{
    public class UserDTO
    {
        public string Id { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public ICollection<int> SkillsIds { get; set; }
    }
}
