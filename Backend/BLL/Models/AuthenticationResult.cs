﻿namespace BLL.Models
{
    public class AuthenticationResult
    {
        public string Token { get; set; }
    }
}
