﻿using AutoMapper;
using BLL.Models.DTOs;
using DAL.Models;
using System.Linq;

namespace BLL.Configurations
{
    public class BLLMappingProfile : Profile
    {
        public BLLMappingProfile()
        {
            CreateMap<Group, GroupDTO>()
                .ForMember(p => p.SkillsIds, c => c.MapFrom(group => group.Skills.Select(x => x.Id)))
                .ReverseMap();

            CreateMap<UserSkill, UserProfileSkillDTO>()
                .ForMember(p => p.Name, c => c.MapFrom(x => x.Skill.Name))
                .ForMember(p => p.Description, c => c.MapFrom(x => x.Skill.Description));

            CreateMap<Skill, SkillDTO>()
                .ReverseMap();

            CreateMap<User, UserDTO>()
                .ForMember(p => p.SkillsIds, c => c.MapFrom(user => user.UserSkills.Select(x => x.SkillId)))
                .ReverseMap();
        }
    }
}
