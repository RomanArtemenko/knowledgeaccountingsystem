﻿using BLL.Models.DTOs;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BLL.Interfaces
{
    public interface IGroupService
    {
        Task AddGroupAsync(GroupDTO group);
        Task DeleteGroupByIdAsync(int groupId);
        Task UpdateGroupAsync(GroupDTO group);
        Task<ICollection<GroupDTO>> GetGroupsAsync();
        Task<ICollection<SkillDTO>> GetSkillsByGroupIdAsync(int groupId);
    }
}
