﻿using BLL.Models.DTOs;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BLL.Interfaces
{
    public interface ISkillService
    {
        Task AddSkillAsync(SkillDTO skill);
        Task DeleteSkillByIdAsync(int skillId);
        Task UpdateSkillAsync(SkillDTO skill);
        Task<ICollection<SkillDTO>> GetSkillsAsync();
    }
}
