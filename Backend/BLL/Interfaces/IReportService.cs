﻿using BLL.Models.DTOs;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BLL.Interfaces
{
    public interface IReportService
    {
        Task<byte[]> GenerateReport(ICollection<UserDTO> users);
    }
}
