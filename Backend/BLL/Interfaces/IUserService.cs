﻿using BLL.Models;
using BLL.Models.DTOs;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BLL.Interfaces
{
    public interface IUserService
    {
        Task RegisterAsync(RegisterModel registerModelDTO);
        Task AddUserToRoleAsync(string userId, string role);
        Task RemoveUserFromRoleAsync(string userId, string role);
        Task<AuthenticationResult> LoginAsync(LoginModel loginData);
        Task<UserDTO> GetUserByEmailAsync(string email);
        Task<UserDTO> GetUserByIdAsync(string userId);
        Task<ICollection<UserDTO>> GetUsersAsync();
        Task DeleteUserByUserIdAsync(string userId);
        Task<UserProfileSkillDTO> GetUserSkillsByUserIdAsync(string userId);
        Task UpdateUserProfileDataAsync(string userId, UserDTO user);
        Task UpdateUserPasswordAsync(string userId, string currentPassword, string newPassword);
        Task UploadUserProfileImageAsync(byte[] fileData, string userId, string fileType, int fileSize);
        Task RemoveUserProfileImageAsync(string userId);
        Task<byte[]> GetUserProfileImageAsync(string userId);
        Task<ICollection<UserDTO>> GetProgrammersBySkillAsync(int skillId, int knowledgeLevel);
        Task AttachSkillToUserAsync(int skillId, string userId, int knowledgeLevel);
        Task RemoveSkillFromUserAsync(int skillId, string userId);
        Task UpdateKnowledgeLevelOfUserSkillAsync(int skillId, string userId, int newKnowledgeLevel);
    }
}
