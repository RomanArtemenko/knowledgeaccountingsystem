﻿using System.Threading.Tasks;

namespace DAL.ImageFileStreamStorage.Interfaces
{
    public interface IFileStorageContext
    {
        Task<byte[]> ReadFile(string fileName);
        Task SaveFile(byte[] fileData, string fileName);
        void DeleteFile(string fileName);
    }
}
