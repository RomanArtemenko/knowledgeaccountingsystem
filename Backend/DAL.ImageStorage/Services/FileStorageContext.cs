﻿using DAL.ImageFileStreamStorage.Configurations;
using DAL.ImageFileStreamStorage.Exceptions;
using DAL.ImageFileStreamStorage.Interfaces;
using Microsoft.Extensions.Options;
using System.IO;
using System.Threading.Tasks;

namespace DAL.ImageStorage.Services
{
    public class FileStorageContext : IFileStorageContext
    {
        private readonly string _path;
        public FileStorageContext(IOptions<FileStorageConfig> options)
        {
            _path = options.Value.BasePath;
        }

        public void DeleteFile(string fileName)
        {
            if (string.IsNullOrEmpty(fileName))
                throw new FileStorageException($"File name is null or empty!");

            if (!File.Exists(Path.Combine(_path, $"{fileName}.jpg")))
                throw new FileStorageException($"File does not exists!");

            File.Delete(Path.Combine(_path, $"{fileName}.jpg"));

        }

        public async Task SaveFile(byte[] fileData, string fileName)
        {
            if (fileData == null)
                throw new FileStorageException($"File data is null!");
            if (string.IsNullOrEmpty(fileName))
                throw new FileStorageException($"File name is null or empty!");

            using var fileStream = new FileStream(Path.Combine(_path, $"{fileName}.jpg"), FileMode.Create);

            await fileStream.WriteAsync(fileData);
        }

        public async Task<byte[]> ReadFile(string fileName)
        {
            if (string.IsNullOrEmpty(fileName))
                throw new FileStorageException($"File name is null or empty!");


            var stream = File.OpenRead(Path.Combine(_path, $"{fileName}.jpg"));

            var buffer = new byte[stream.Length];
            await stream.ReadAsync(buffer, 0, buffer.Length).ConfigureAwait(false);
            return buffer;
        }
    }
}
